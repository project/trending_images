<?php
namespace Drupal\trending_images\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\trending_images\TrendingImagesService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Processes banners and handles publishing.
 *
 * @QueueWorker(
 *   id = "media_processing_queue",
 *   title = @Translation("Media processing queue"),
 *   cron = {"time" = 60}
 * )
 */
class MediaProcessingQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * EntityTypeManager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * EntityTypeManager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $trendingImageService;


  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,EntityTypeManager $entityTypeManager, TrendingImagesService $tendingImagesService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->trendingImageService = $tendingImagesService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('trending_images.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $image = $data['image'];
    $link = $image->link;
    $likes = $image->likes->count;
    $comments = $image->comments->count;

    $fetchedImage = $this->trendingImageService->fetchImageFromUlr($image->images->standard_resolution, $data['settings']);
    $loadedEntities = $this->entityTypeManager->getStorage($data['data_bundle']['field_type'])->loadMultiple($data['data_bundle']['entityFieldResult']);

    foreach ($loadedEntities as $loadedEntity){
      $fieldValues = $loadedEntity->get($data["data_bundle"]["machine_name"])->getValue();

      foreach ($fieldValues as $valueKey => $value){
        if($value["source_link"] == $link){
          return true;
        }
      }

      $formattedField = [
        'target_id' => $fetchedImage->id(),
        'source_link' => $link,
        'likes' => $likes,
        'comments' => $comments,
        'width' => $image->images->standard_resolution->width,
        'height' => $image->images->standard_resolution->height,
        'permanent' => 0,
        'value' => $this->pluginDefinition['channel'],
      ];

      if(count($fieldValues) == $data["data_bundle"]['cardinality']){
        array_shift($fieldValues);
      }
      array_push($fieldValues, $formattedField);

      $loadedEntity->set($data['data_bundle']['machine_name'],$fieldValues);
      $loadedEntity->save();
    }
    return true;
  }
}
