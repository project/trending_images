<?php

namespace Drupal\trending_images;

//use Drupal\trending_images\Plugin\Commerce\EntityTrait\EntityTraitInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
* Defines the interface for commerce_entity_trait plugin managers.
*/
interface TrendingImagesManagerInterface extends PluginManagerInterface {

}
